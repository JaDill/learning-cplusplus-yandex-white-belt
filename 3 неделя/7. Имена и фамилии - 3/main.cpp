#include <iostream>
#include <cstring>
#include <vector>
#include <map>
#include <algorithm>
#include <set>
#include <cmath>
#include <unordered_set>

class Person
{
public:
    Person(const std::string& name, const std::string& surname, int dob)
    {
        _n[dob] = name;
        _s[dob] = surname;
        _dob = dob;
    }

    void ChangeFirstName(int year, const std::string& first_name)
    {
        if (year < _dob)
            return;
        _n[year] = first_name;
    }

    void ChangeLastName(int year, const std::string& last_name)
    {
        if (year < _dob)
            return;
        _s[year] = last_name;
    }

    std::string GetFullName(int year) const
    {
        if (year < _dob)
            return "No person";
        auto n = _n.lower_bound(year), s = _s.lower_bound(year);
        if (n != _n.end() && s != _s.end())
            return n->second + " " + s->second;
        else if (n != _n.end() && s == _s.end())
            return n->second + " with unknown last name";
        else if (n == _n.end() && s != _s.end())
            return s->second + " with unknown first name";
        else
            return "Incognito";
    }

    std::string GetFullNameWithHistory(int year) const
    {
        if (year < _dob)
            return "No person";
        auto n = _n.lower_bound(year), s = _s.lower_bound(year);
        std::string result;
        if (n != _n.end() && s != _s.end())
        {
            result = n->second + " ";
            std::string tempName = n->second;

            std::vector<std::string> name;
            for (n++; n != _n.end(); ++n)
                if (n->second != tempName)
                {
                    name.push_back(n->second);
                    tempName = n->second;
                }
            if (!name.empty())
            {
                result += "(";
                for (auto& str: name)
                    result += (str + ", ");
                result.resize(result.size() - 2);
                result += ") ";
            }

            result += s->second + "";
            tempName = s->second;

            std::vector<std::string> surname;
            for (s++; s != _s.end(); ++s)
                if (s->second != tempName)
                {
                    surname.push_back(s->second);
                    tempName = s->second;
                }
            if (!surname.empty())
            {
                result += " (";
                for (auto& str: surname)
                    result += (str + ", ");
                result.resize(result.size() - 2);
                result += ")";
            }


        } else if (n != _n.end() && s == _s.end())
        {
            result = n->second + " ";
            std::string tempName = n->second;

            std::vector<std::string> name;
            for (n++; n != _n.end(); ++n)
                if (n->second != tempName)
                {
                    name.push_back(n->second);
                    tempName = n->second;
                }
            if (!name.empty())
            {
                result += "(";
                for (auto& str: name)
                    result += (str + ", ");
                result.resize(result.size() - 2);
                result += ") ";
            }
            result += "with unknown last name";
        } else if (n == _n.end() && s != _s.end())
        {
            result = s->second + " ";
            std::string tempName = s->second;

            std::vector<std::string> surname;
            for (s++; s != _s.end(); ++s)
                if (s->second != tempName)
                {
                    surname.push_back(s->second);
                    tempName = s->second;
                }
            if (!surname.empty())
            {
                result += "(";
                for (auto& str: surname)
                    result += (str + ", ");
                result.resize(result.size() - 2);
                result += ") ";
            }
            result += "with unknown first name";
        } else
            return "Incognito";
        return result;
    }

private:
    std::map<int, std::string, std::greater<int>> _n;
    std::map<int, std::string, std::greater<int>> _s;
    int _dob;
};