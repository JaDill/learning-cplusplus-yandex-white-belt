#include <iostream>
#include <cstring>
#include <vector>
#include <map>
#include <algorithm>
#include <set>
#include <cmath>


int main()
{
    int n;
    std::cin >> n;
    std::vector<std::string> a(n);
    for (int i = 0; i < n; ++i)
        std::cin >> a[i];
    std::sort(a.begin(), a.end(), [](const std::string& x, const std::string& y)
    {
        for (int i = 0; i < x.size() && i < y.size(); ++i)
            if (std::tolower(x[i]) != std::tolower(y[i]))
                return std::tolower(x[i]) < std::tolower(y[i]);
        return x.size() < y.size();
    });

    for (int i = 0; i < n; ++i)
        std::cout << a[i] << " ";
    return 0;
}