#include <iostream>
#include <cstring>
#include <vector>
#include <map>
#include <algorithm>
#include <set>
#include <cmath>
#include <unordered_set>

class ReversibleString
{
private:
    std::string _str;
public:
    ReversibleString() : _str(""){}

    ReversibleString(const std::string& str) : _str(str){}

    void Reverse()
    {
        std::reverse(_str.begin(), _str.end());
    }

    const std::string& ToString() const
    {
        return _str;
    }
};