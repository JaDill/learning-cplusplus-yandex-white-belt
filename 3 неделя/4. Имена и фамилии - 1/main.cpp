#include <iostream>
#include <cstring>
#include <vector>
#include <map>
#include <algorithm>
#include <set>
#include <cmath>

class Person
{
public:
    void ChangeFirstName(int year, const std::string& first_name)
    {
        _n[year] = first_name;
    }

    void ChangeLastName(int year, const std::string& last_name)
    {
        _s[year] = last_name;
    }

    std::string GetFullName(int year)
    {
        auto n = _n.lower_bound(year), s = _s.lower_bound(year);
        if (n != _n.end() && s != _s.end())
            return n->second + " " + s->second;
        else if (n != _n.end() && s == _s.end())
            return n->second + " with unknown last name";
        else if (n == _n.end() && s != _s.end())
            return s->second + " with unknown first name";
        else
            return "Incognito";
    }

private:
    std::map<int, std::string, std::greater<int>> _n;
    std::map<int, std::string, std::greater<int>> _s;
};