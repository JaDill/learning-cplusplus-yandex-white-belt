#include <iostream>
#include <cstring>
#include <vector>
#include <map>
#include <algorithm>
#include <set>
#include <cmath>

class SortedStrings
{
private:
    std::multiset<std::string> _words;
public:
    void AddString(const std::string& s)
    {
        _words.insert(s);
    }

    std::vector<std::string> GetSortedStrings()
    {
        return std::vector<std::string>(_words.begin(), _words.end());
    }
};
