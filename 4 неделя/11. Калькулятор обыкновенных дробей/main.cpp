#include <iostream>
#include <cmath>
#include <sstream>
#include <vector>
#include <set>
#include <map>
#include <stdexcept>
#include <exception>

class Rational
{
public:
    Rational()
    {
        _numerator = 0;
        _denominator = 1;
    }

    Rational(int numerator, int denominator)
    {
        makeRational(numerator, denominator);
    }

    int Numerator() const
    {
        return _numerator;
    }

    int Denominator() const
    {
        return _denominator;
    }

    friend bool operator==(const Rational& lhv, const Rational& rhv)
    {
        return lhv._numerator == rhv._numerator && lhv._denominator == rhv._denominator;
    }

    friend Rational operator+(const Rational& lhv, const Rational& rhv)
    {
        int commonNumerator = lhv._numerator * rhv._denominator + rhv._numerator * lhv._denominator;
        int commonDenominator = lhv._denominator * rhv._denominator;
        return {commonNumerator, commonDenominator};
    }

    friend Rational operator-(const Rational& lhv, const Rational& rhv)
    {
        int commonNumerator = lhv._numerator * rhv._denominator - rhv._numerator * lhv._denominator;
        int commonDenominator = lhv._denominator * rhv._denominator;
        return {commonNumerator, commonDenominator};
    }

    friend Rational operator*(const Rational& lhv, const Rational& rhv)
    {
        int commonNumerator = lhv._numerator * rhv._numerator;
        int commonDenominator = lhv._denominator * rhv._denominator;
        return {commonNumerator, commonDenominator};
    }

    friend Rational operator/(const Rational& lhv, const Rational& rhv)
    {
        int commonNumerator = lhv._numerator * rhv._denominator;
        int commonDenominator = lhv._denominator * rhv._numerator;
        if (commonDenominator == 0)
            throw std::domain_error("Division by zero");
        return {commonNumerator, commonDenominator};
    }

    friend std::istream& operator>>(std::istream& in, Rational& value)
    {
        if (!in)
            return in;
        int n = value._numerator, d = value._denominator;
        in >> n;
        in.ignore(1);
        in >> d;
        value.makeRational(n, d);
        return in;
    }

    friend std::ostream& operator<<(std::ostream& out, const Rational& value)
    {
        out << value._numerator << "/" << value._denominator;
        return out;
    }

    friend bool operator<(const Rational& lhv, const Rational& rhv)
    {
        if (lhv._denominator == rhv._denominator)
            return lhv._numerator < rhv._numerator;
        int firstNumerator = lhv._numerator * rhv._denominator;
        int secondNumerator = rhv._numerator * lhv._denominator;
        return firstNumerator < secondNumerator;
    }

private:
    int nod(int a, int b)
    {
        if (a == 0)
            return b;

        b %= a;
        nod(b, a);
    }

    void makeRational(int numerator, int denominator)
    {
        if (denominator == 0)
            throw std::invalid_argument("Invalid argument");
        int nod = this->nod(numerator, denominator);
        numerator /= nod;
        denominator /= nod;
        _numerator = numerator;
        if (std::abs(denominator) != denominator)
            _numerator *= -1;
        _denominator = std::abs(denominator);
    }

private:
    int _numerator;
    unsigned int _denominator;
};

int main()
{
    Rational f, s;
    char oper;
    try
    {
        std::cin >> f >> oper >> s;
        switch (oper)
        {
            case '+':
                f = f + s;
                break;
            case '-':
                f = f - s;
                break;
            case '*':
                f = f * s;
                break;
            case '/':
                f = f / s;
                break;
        }
        std::cout << f;
    } catch (std::invalid_argument& ex)
    {
        std::cout << ex.what() << "\n";
    } catch (std::domain_error& ex)
    {
        std::cout << ex.what() << "\n";
    }

    return 0;
}