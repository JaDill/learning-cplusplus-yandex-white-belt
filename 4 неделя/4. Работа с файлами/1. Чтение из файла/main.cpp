#include <iostream>
#include <cstring>
#include <vector>
#include <map>
#include <algorithm>
#include <set>
#include <cmath>
#include <unordered_set>
#include <fstream>

int main()
{
    std::ifstream fin("input.txt");
    std::string line;
    while (std::getline(fin, line))
    {
        std::cout << line << "\n";
    }
    fin.close();

    return 0;
}