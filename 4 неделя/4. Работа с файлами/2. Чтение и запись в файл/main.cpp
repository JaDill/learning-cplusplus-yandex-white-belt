#include <iostream>
#include <cstring>
#include <vector>
#include <map>
#include <algorithm>
#include <set>
#include <cmath>
#include <unordered_set>
#include <fstream>

int main()
{
    std::ifstream fin("input.txt");
    std::ofstream fout("output.txt");
    std::string line;
    while (std::getline(fin, line))
    {
        fout << line << "\n";
    }
    fin.close();
    fout.close();

    return 0;
}