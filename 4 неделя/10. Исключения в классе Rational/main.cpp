#include <iostream>
#include <cmath>
#include <sstream>
#include <vector>
#include <set>
#include <map>
#include <stdexcept>
#include <exception>

class Rational
{
public:
    Rational()
    {
        _numerator = 0;
        _denominator = 1;
    }

    Rational(int numerator, int denominator)
    {
        makeRational(numerator, denominator);
    }

    int Numerator() const
    {
        return _numerator;
    }

    int Denominator() const
    {
        return _denominator;
    }

    friend bool operator==(const Rational& lhv, const Rational& rhv)
    {
        return lhv._numerator == rhv._numerator && lhv._denominator == rhv._denominator;
    }

    friend Rational operator+(const Rational& lhv, const Rational& rhv)
    {
        int commonNumerator = lhv._numerator * rhv._denominator + rhv._numerator * lhv._denominator;
        int commonDenominator = lhv._denominator * rhv._denominator;
        return {commonNumerator, commonDenominator};
    }

    friend Rational operator-(const Rational& lhv, const Rational& rhv)
    {
        int commonNumerator = lhv._numerator * rhv._denominator - rhv._numerator * lhv._denominator;
        int commonDenominator = lhv._denominator * rhv._denominator;
        return {commonNumerator, commonDenominator};
    }

    friend Rational operator*(const Rational& lhv, const Rational& rhv)
    {
        int commonNumerator = lhv._numerator * rhv._numerator;
        int commonDenominator = lhv._denominator * rhv._denominator;
        return {commonNumerator, commonDenominator};
    }

    friend Rational operator/(const Rational& lhv, const Rational& rhv)
    {
        int commonNumerator = lhv._numerator * rhv._denominator;
        int commonDenominator = lhv._denominator * rhv._numerator;
        if (commonDenominator == 0)
            throw std::domain_error("");
        return {commonNumerator, commonDenominator};
    }

    friend std::istream& operator>>(std::istream& in, Rational& value)
    {
        if (!in)
            return in;
        int n = value._numerator, d = value._denominator;
        in >> n;
        in.ignore(1);
        in >> d;
        value.makeRational(n, d);
        return in;
    }

    friend std::ostream& operator<<(std::ostream& out, const Rational& value)
    {
        out << value._numerator << "/" << value._denominator;
        return out;
    }

    friend bool operator<(const Rational& lhv, const Rational& rhv)
    {
        if (lhv._denominator == rhv._denominator)
            return lhv._numerator < rhv._numerator;
        int firstNumerator = lhv._numerator * rhv._denominator;
        int secondNumerator = rhv._numerator * lhv._denominator;
        return firstNumerator < secondNumerator;
    }

private:
    int nod(int a, int b)
    {
        if (a == 0)
            return b;

        b %= a;
        nod(b, a);
    }

    void makeRational(int numerator, int denominator)
    {
        if (denominator == 0)
            throw std::invalid_argument("");
        int nod = this->nod(numerator, denominator);
        numerator /= nod;
        denominator /= nod;
        _numerator = numerator;
        if (std::abs(denominator) != denominator)
            _numerator *= -1;
        _denominator = std::abs(denominator);
    }

private:
    int _numerator;
    unsigned int _denominator;
};

int main()
{
    {
        const Rational r(3, 10);
        if (r.Numerator() != 3 || r.Denominator() != 10)
        {
            std::cout << "Rational(3, 10) != 3/10" << "\n";
            return 1;
        }
    }

    {
        const Rational r(8, 12);
        if (r.Numerator() != 2 || r.Denominator() != 3)
        {
            std::cout << "Rational(8, 12) != 2/3" << "\n";
            return 2;
        }
    }

    {
        const Rational r(-4, 6);
        if (r.Numerator() != -2 || r.Denominator() != 3)
        {
            std::cout << "Rational(-4, 6) != -2/3" << "\n";
            return 3;
        }
    }

    {
        const Rational r(4, -6);
        if (r.Numerator() != -2 || r.Denominator() != 3)
        {
            std::cout << "Rational(4, -6) != -2/3" << "\n";
            return 3;
        }
    }

    {
        const Rational r(0, 15);
        if (r.Numerator() != 0 || r.Denominator() != 1)
        {
            std::cout << "Rational(0, 15) != 0/1" << "\n";
            return 4;
        }
    }

    {
        const Rational defaultConstructed;
        if (defaultConstructed.Numerator() != 0 || defaultConstructed.Denominator() != 1)
        {
            std::cout << "Rational() != 0/1" << "\n";
            return 5;
        }
    }

    {
        Rational r1(4, 6);
        Rational r2(2, 3);
        bool equal = r1 == r2;
        if (!equal)
        {
            std::cout << "4/6 != 2/3" << "\n";
            return 1;
        }
    }

    {
        Rational a(2, 3);
        Rational b(4, 3);
        Rational c = a + b;
        bool equal = c == Rational(2, 1);
        if (!equal)
        {
            std::cout << "2/3 + 4/3 != 2" << "\n";
            return 2;
        }
    }

    {
        Rational a(5, 7);
        Rational b(2, 9);
        Rational c = a - b;
        bool equal = c == Rational(31, 63);
        if (!equal)
        {
            std::cout << "5/7 - 2/9 != 31/63" << "\n";
            return 3;
        }
    }

    {
        Rational a(2, 3);
        Rational b(4, 3);
        Rational c = a * b;
        bool equal = c == Rational(8, 9);
        if (!equal)
        {
            std::cout << "2/3 * 4/3 != 8/9" << "\n";
            return 1;
        }
    }

    {
        Rational a(5, 4);
        Rational b(15, 8);
        Rational c = a / b;
        bool equal = c == Rational(2, 3);
        if (!equal)
        {
            std::cout << "5/4 / 15/8 != 2/3" << "\n";
            return 2;
        }
    }

    {
        std::ostringstream output;
        output << Rational(-6, 8);
        if (output.str() != "-3/4")
        {
            std::cout << "Rational(-6, 8) should be written as \"-3/4\"" << "\n";
            return 1;
        }
    }

    {
        std::istringstream input("5/7");
        Rational r;
        input >> r;
        bool equal = r == Rational(5, 7);
        if (!equal)
        {
            std::cout << "5/7 is incorrectly read as " << r << "\n";
            return 2;
        }
    }

    {
        std::istringstream input("5/7 10/8");
        Rational r1, r2;
        input >> r1 >> r2;
        bool correct = r1 == Rational(5, 7) && r2 == Rational(5, 4);
        if (!correct)
        {
            std::cout << "Multiple values are read incorrectly: " << r1 << " " << r2 << "\n";
            return 3;
        }

        input >> r1;
        input >> r2;
        correct = r1 == Rational(5, 7) && r2 == Rational(5, 4);
        if (!correct)
        {
            std::cout << "Read from empty stream shouldn't change arguments: " << r1 << " " << r2 << "\n";
            return 4;
        }
    }

    {
        const std::set<Rational> rs = {{1, 2},
                                       {1, 25},
                                       {3, 4},
                                       {3, 4},
                                       {1, 2}};
        if (rs.size() != 3)
        {
            std::cout << "Wrong amount of items in the set" << "\n";
            return 1;
        }

        std::vector<Rational> v;
        for (auto x : rs)
        {
            v.push_back(x);
        }
        if (v != std::vector<Rational>{{1, 25},
                                       {1, 2},
                                       {3, 4}})
        {
            std::cout << "Rationals comparison works incorrectly" << "\n";
            return 2;
        }
    }

    {
        std::map<Rational, int> count;
        ++count[{1, 2}];
        ++count[{1, 2}];

        ++count[{2, 3}];

        if (count.size() != 2)
        {
            std::cout << "Wrong amount of items in the map" << "\n";
            return 3;
        }
    }

    try
    {
        Rational r(1, 0);
        std::cout << "Doesn't throw in case of zero denominator" << "\n";
        return 1;
    } catch (std::invalid_argument&)
    {
    }

    try
    {
        auto x = Rational(1, 2) / Rational(0, 1);
        std::cout << "Doesn't throw in case of division by zero" << "\n";
        return 2;
    } catch (std::domain_error&)
    {
    }


    std::cout << "OK" << "\n";
    return 0;
}