#include <iostream>
#include <cstring>
#include <vector>
#include <map>
#include <algorithm>
#include <set>
#include <cmath>
#include <unordered_set>
#include <fstream>
#include <iomanip>

int main()
{
    std::ifstream fin("input.txt");
    int n, m;
    fin >> n >> m;
    int number;
    for (int i = 0; i < n; ++i)
    {
        for (int j = 0; j < m; ++j)
        {
            fin >> number;
            if (j + 1 != m)
                fin.ignore(1);
            std::cout << std::setw(10) << number;
            if (j + 1 != m)
                std::cout << " ";
        }
        //if (i + 1 != n)
        std::cout << "\n";
    }
    return 0;
}