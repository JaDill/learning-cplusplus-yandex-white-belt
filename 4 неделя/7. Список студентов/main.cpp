#include <iostream>
#include <cstring>
#include <vector>
#include <map>
#include <algorithm>
#include <set>
#include <cmath>
#include <unordered_set>
#include <fstream>
#include <iomanip>

struct Student
{
    std::string name;
    std::string surname;
    int day;
    int month;
    int year;
};

int main()
{
    int n;
    std::cin >> n;
    std::vector<Student> group(n);
    std::string name, surname;
    int day, month, year;
    for (int i = 0; i < n; ++i)
    {
        std::cin >> name >> surname >> day >> month >> year;
        group[i] = {name, surname, day, month, year};
    }
    std::cin >> n;
    std::string command;
    int number;
    for (int i = 0; i < n; ++i)
    {
        std::cin >> command >> number;
        if (number > 0 && number <= group.size())
        {
            if (command == "name")
            {
                std::cout<<group[number-1].name<<" "<<group[number-1].surname<<"\n";
                continue;
            } else if (command == "date")
            {
                std::cout<<group[number-1].day<<"."<<group[number-1].month<<"."<<group[number-1].year<<"\n";
                continue;
            }
        }
        std::cout << "bad request\n";

    }
    return 0;
}