#include <iostream>
#include <cstring>
#include <vector>
#include <map>
#include <algorithm>
#include <set>
#include <cmath>
#include <unordered_set>

struct Specialization
{
    std::string _value;

    explicit Specialization(const std::string& value)
    {
        _value = value;
    }
};

struct Course
{
    std::string _value;

    explicit Course(const std::string& value)
    {
        _value = value;
    }
};

struct Week
{
    std::string _value;

    explicit Week(const std::string& value)
    {
        _value = value;
    }
};

struct LectureTitle
{
    std::string specialization;
    std::string course;
    std::string week;

    explicit LectureTitle(const Specialization& specializationn, const Course& coursen, const Week& weekn)
    {
        specialization = specializationn._value;
        course = coursen._value;
        week = weekn._value;
    }
};

int main()
{

    return 0;
}