#include <iostream>
#include <cstring>
#include <vector>
#include <map>
#include <algorithm>
#include <set>
#include <cmath>
#include <unordered_set>
#include <fstream>
#include <iomanip>

int main()
{
    std::ifstream fin("input.txt");
    double line;
    std::cout << std::fixed << std::setprecision(3);
    while (fin >> line)
    {
        std::cout << line << "\n";
    }
    fin.close();

    return 0;
}