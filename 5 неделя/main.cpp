#include <iostream>
#include <exception>
#include <cstring>
#include <set>
#include <map>
#include <algorithm>
#include <regex>
#include <iomanip>
#include <fstream>


class Date
{
public:
    Date() : _day(INT32_MAX), _month(INT32_MAX), _year(INT32_MAX) {}

    Date(const std::string& datestr)
    {
        makeDate(datestr);
    }

public:
    friend std::istream& operator>>(std::istream& in, Date& date)
    {
        std::string datestr;
        in >> datestr;
        date.makeDate(datestr);
        return in;
    }

    friend std::ostream& operator<<(std::ostream& out, const Date& date)
    {
        out << std::setw(4) << std::setfill('0') << date._year << "-" <<
            std::setw(2) << std::setfill('0') << date._month << "-" <<
            std::setw(2) << std::setfill('0') << date._day;
        return out;
    }

    friend bool operator<(const Date& lhv, const Date& rhv)
    {
        if (lhv._year == rhv._year)
        {
            if (lhv._month == rhv._month)
                return lhv._day < rhv._day;
            return lhv._month < rhv._month;
        }
        return lhv._year < rhv._year;
    }

public:
    int getYear() const
    {
        return _year;
    }

    int getMonth() const
    {
        return _month;
    }

    int getDay() const
    {
        return _day;
    }

private:
    void makeDate(const std::string& datestr)
    {
        bool needThrow = datestr.empty();

        std::stringstream ss(datestr);
        char def1, def2;
        ss >> _year;
        needThrow = needThrow ? true : ss.eof();
        ss >> def1;
        needThrow = needThrow ? true : ss.eof();
        ss >> _month;
        needThrow = needThrow ? true : ss.eof();
        ss >> def2;
        needThrow = needThrow ? true : ss.eof();
        ss >> _day;
        needThrow = needThrow ? true : !ss.eof();

        if (needThrow || def1 != '-' || def2 != '-' || _month == INT32_MAX || _day == INT32_MAX)
            throw std::invalid_argument("Wrong date format: " + datestr);

        if (_month < 1 || _month > 12)
            throw std::invalid_argument("Month value is invalid: " + std::to_string(_month));

        if (_day < 1 || _day > 31)
            throw std::invalid_argument("Day value is invalid: " + std::to_string(_day));
    }

private:
    int _day, _month, _year;
};

class Database
{
public:
    void AddEvent(const Date& date, const std::string& event)
    {
        calendar[date].insert(event);
    }

    bool DeleteEvent(const Date& date, const std::string& event)
    {
        return calendar[date].erase(event);
    }

    int DeleteDate(const Date& date)
    {
        size_t count = calendar[date].size();
        calendar[date].clear();
        return count;
    }

    void Find(const Date& date) const
    {
        const auto it = calendar.find(date);
        if (it != calendar.end())
        {
            for (const auto& event : it->second)
            {
                std::cout << event << "\n";
            }
        }
    }

    void Print() const
    {
        for (const auto& i : calendar)
            for (const auto& event : i.second)
                std::cout << i.first << " " << event << "\n";
    }

private:
    std::map<Date, std::set<std::string>> calendar;
};

int main()
{
    Database db;

    std::string commandline, commandstr, eventstr;
    try
    {
        while (getline(std::cin, commandline))
        {
            if (commandline.empty())
                continue;

            std::stringstream sscommand(commandline);
            sscommand >> commandstr;

            if (commandstr == "Add")
            {
                Date date;
                sscommand >> date >> eventstr;
                db.AddEvent(date, eventstr);
            } else if (commandstr == "Del")
            {
                eventstr = "";
                Date date;
                sscommand >> date >> eventstr;
                if (eventstr.empty())
                {
                    int count = db.DeleteDate(date);
                    std::cout << "Deleted " << count << " events" << "\n";
                } else
                {
                    bool success = db.DeleteEvent(date, eventstr);
                    if (success)
                        std::cout << "Deleted successfully";
                    else
                        std::cout << "Event not found";
                    std::cout << "\n";
                }
            } else if (commandstr == "Find")
            {
                Date date;
                sscommand >> date;
                db.Find(date);
            } else if (commandstr == "Print")
            {
                db.Print();
            } else
            {
                std::cout << "Unknown command: " << commandstr << "\n";
            }
        }
    } catch (std::invalid_argument& ex)
    {
        std::cout << ex.what();
        return 0;
    }

    return 0;
}