#include <iostream>
#include <cstring>
#include <vector>
#include <algorithm>

std::vector<int> Reversed(const std::vector<int>& v0)
{
    std::vector<int> v(v0);
    std::reverse(v.begin(), v.end());
    return v;
}

int main()
{
    std::vector<int> a;
    Reversed(a);
    return 0;
}