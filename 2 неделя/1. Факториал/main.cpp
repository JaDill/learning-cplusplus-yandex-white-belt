#include <iostream>
#include <vector>
#include <algorithm>

int Factorial(int num)
{
    if (num < 2)
        return 1;
    int result = 1;
    for (int i = 2; i <= num; ++i)
    {
        result *= i;
    }
    return result;
}

int main()
{
    int a;
    std::cin >> a;
    std::cout << Factorial(a);
    return 0;
}