#include <iostream>
#include <cstring>
#include <vector>
#include <map>
#include <algorithm>


int main()
{
    int n;
    std::cin >> n;
    int number = 1;
    std::map<std::vector<std::string>, int> pointsToBus;
    for (int i = 0; i < n; ++i)
    {
        int count;
        std::cin >> count;
        std::vector<std::string> temp(count);
        for (int j = 0; j < count; ++j)
        {
            std::cin >> temp[j];
        }
        if (pointsToBus.find(temp) != pointsToBus.end())
            std::cout << "Already exists for " << pointsToBus.find(temp)->second << "\n";
        else
        {
            pointsToBus[temp] = number++;
            std::cout << "New bus " << number - 1 << "\n";
        }
    }

    return 0;
}