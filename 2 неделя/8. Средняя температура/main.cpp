#include <iostream>
#include <cstring>
#include <vector>
#include <algorithm>


int main()
{
    int n;
    std::cin >> n;
    std::vector<int> a(n);
    int ave = 0;
    for (int i = 0; i < n; ++i)
    {
        std::cin >> a[i];
        ave += a[i];
    }
    ave /= a.size();
    std::vector<int> b;
    for (int i = 0; i < a.size(); ++i)
        if (a[i] > ave)
            b.push_back(i);

    std::cout << b.size() << "\n";
    for (int i:b)
        std::cout << i << " ";

    return 0;
}