#include <iostream>
#include <cstring>
#include <vector>
#include <map>
#include <algorithm>
#include <set>


int main()
{
    int n;
    std::cin >> n;
    int number = 1;
    std::map<std::set<std::string>, int> pointsToBus;
    for (int i = 0; i < n; ++i)
    {
        int count;
        std::cin >> count;
        std::set<std::string> temp;
        for (int j = 0; j < count; ++j)
        {
            std::string t;
            std::cin >> t;
            temp.insert(t);
        }
        if (pointsToBus.find(temp) != pointsToBus.end())
            std::cout << "Already exists for " << pointsToBus.find(temp)->second << "\n";
        else
        {
            pointsToBus[temp] = number++;
            std::cout << "New bus " << number - 1 << "\n";
        }
    }

    return 0;
}