#include <iostream>
#include <cstring>
#include <vector>
#include <map>
#include <algorithm>


int main()
{
    int n;
    std::cin >> n;
    for (int i = 0; i < n; ++i)
    {
        std::string a, b;
        std::cin >> a >> b;
        std::map<char, int> a1, b1;
        for (char& ch : a)
            a1[ch]++;
        for (char& ch : b)
            b1[ch]++;
        if (a1 == b1)
            std::cout << "YES\n";
        else
            std::cout << "NO\n";
    }

    return 0;
}