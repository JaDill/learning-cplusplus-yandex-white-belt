#include <iostream>
#include <cstring>
#include <vector>
#include <map>
#include <algorithm>


int main()
{
    int n;
    std::cin >> n;
    std::map<std::string, std::vector<std::string>> busToPoints, pointToBus;
    for (int i = 0; i < n; ++i)
    {
        std::string command;
        std::cin >> command;
        if (command == "NEW_BUS")
        {
            std::string name;
            int countStops;
            std::cin >> name >> countStops;
            for (int i = 0; i < countStops; ++i)
            {
                std::string nameStop;
                std::cin >> nameStop;
                busToPoints[name].push_back(nameStop);
                pointToBus[nameStop].push_back(name);
            }
        } else if (command == "BUSES_FOR_STOP")
        {
            std::string nameStop;
            std::cin >> nameStop;
            if (pointToBus.find(nameStop) == pointToBus.end())
                std::cout << "No stop";
            else
            {
                for (int i = 0; i < pointToBus[nameStop].size(); ++i)
                {
                    std::cout << pointToBus[nameStop][i] << " ";
                }
            }
            std::cout << "\n";
        } else if (command == "STOPS_FOR_BUS")
        {
            std::string name;
            std::cin >> name;
            if (busToPoints.find(name) == busToPoints.end())
                std::cout << "No bus\n";
            else
            {
                for (int i = 0; i < busToPoints[name].size(); ++i)
                {
                    std::cout << "Stop " << busToPoints[name][i] << ": ";
                    if (pointToBus[busToPoints[name][i]].size() == 1)
                        std::cout << "no interchange";
                    else
                        for (int j = 0; j < pointToBus[busToPoints[name][i]].size(); ++j)
                        {
                            if (pointToBus[busToPoints[name][i]][j] == name)
                                continue;
                            std::cout << pointToBus[busToPoints[name][i]][j] << " ";
                        }
                    std::cout << "\n";
                }
            }
        } else
        {
            if (busToPoints.empty())
                std::cout << "No buses\n";
            else
                for (const auto& it : busToPoints)
                {
                    std::cout << "Bus " << it.first << ": ";
                    for (int i = 0; i < it.second.size(); ++i)
                        std::cout << it.second[i] << " ";
                    std::cout << "\n";
                }
        }
    }

    return 0;
}