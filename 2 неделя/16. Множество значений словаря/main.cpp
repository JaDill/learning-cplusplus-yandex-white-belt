#include <iostream>
#include <cstring>
#include <vector>
#include <map>
#include <algorithm>
#include <set>


std::set<std::string> BuildMapValuesSet(const std::map<int, std::string>& m)
{
    std::set<std::string> result;
    for (auto it = m.begin(); it != m.end(); ++it)
        result.insert(it->second);
    return result;
}