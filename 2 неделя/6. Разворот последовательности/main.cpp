#include <iostream>
#include <cstring>
#include <vector>
#include <algorithm>

void Reverse(std::vector<int>& v)
{
    std::reverse(v.begin(), v.end());
}

int main()
{
    std::vector<int> a;
    Reverse(a);
    return 0;
}