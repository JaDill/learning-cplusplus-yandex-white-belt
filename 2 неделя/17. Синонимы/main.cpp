#include <iostream>
#include <cstring>
#include <vector>
#include <map>
#include <algorithm>
#include <set>


int main()
{
    int n;
    std::cin >> n;
    std::map<std::string, std::set<std::string>> dict;
    std::string command;
    for (int i = 0; i < n; ++i)
    {
        std::cin >> command;
        if (command == "ADD")
        {
            std::string s1, s2;
            std::cin >> s1 >> s2;
            dict[s1].insert(s2);
            dict[s2].insert(s1);
        } else if (command == "COUNT")
        {
            std::string s;
            std::cin >> s;
            if (dict.find(s) != dict.end())
                std::cout << dict[s].size();
            else
                std::cout << 0;
            std::cout << "\n";
        } else
        {
            std::string s1, s2;
            std::cin >> s1 >> s2;
            if (dict.find(s1) != dict.end() && dict[s1].find(s2) != dict[s1].end() ||
                dict.find(s2) != dict.end() && dict[s2].find(s1) != dict[s2].end())
                std::cout << "YES";
            else
                std::cout << "NO";
            std::cout << "\n";
        }
    }


    return 0;
}