#include <iostream>
#include <cstring>
#include <vector>
#include <algorithm>


int main()
{
    const std::vector<int> NUMBER = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
    int n;
    std::cin >> n;

    std::string command, name;
    int day;

    int month = 0;
    std::vector<std::vector<std::string>> plan(32);
    for (int i = 0; i < n; ++i)
    {
        std::cin >> command;
        if (command == "ADD")
        {
            std::cin >> day >> name;
            if (day <= NUMBER[month % 12])
                plan[day].push_back(name);
            else
                plan[NUMBER[month % 12]].push_back(name);

        } else if (command == "NEXT")
        {
            //plan.assign(NUMBER[(++month) % 12], std::vector<std::string>());
            ++month;
            for (int i = NUMBER[month % 12] + 1; i < 32; ++i)
                plan[NUMBER[month%12]].insert(plan[NUMBER[month%12]].end(),plan[i].begin(),plan[i].end());
            plan.erase(plan.begin() + NUMBER[month % 12] + 1, plan.end());
            plan.resize(32);
        } else if (command == "DUMP")
        {
            std::cin >> day;

            std::cout << plan[day].size() << " ";
            for (std::string& str : plan[day])
                std::cout << str << " ";

            std::cout << "\n";
        }
    }

    return 0;
}