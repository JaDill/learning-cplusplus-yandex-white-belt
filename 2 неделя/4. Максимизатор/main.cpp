#include <iostream>
#include <cstring>
#include <vector>

void UpdateIfGreater(int first, int& second)
{
    second = first > second ? first : second;
}

int main()
{
    int a, b;
    std::cin >> a >> b;
    UpdateIfGreater(a, b);
    return 0;
}