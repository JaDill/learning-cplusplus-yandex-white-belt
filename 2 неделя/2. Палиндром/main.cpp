#include <iostream>
#include <cstring>
#include <algorithm>

bool IsPalindrom(std::string str)
{
    for (int i = 0; i < str.size() / 2; ++i)
        if (str[i] != str[str.size() - i - 1])
            return false;
    return true;
}

int main()
{
    std::string str;
    std::cin >> str;
    std::cout << IsPalindrom(str);
    return 0;
}