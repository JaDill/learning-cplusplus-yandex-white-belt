#include <iostream>
#include <cstring>
#include <vector>
#include <map>
#include <algorithm>
#include <set>


int main()
{
    int n;
    std::cin >> n;
    int number = 1;
    std::set<std::string> words;
    for (int i = 0; i < n; ++i)
    {
        std::string temp;
        std::cin >> temp;
        words.insert(temp);
    }
    std::cout << words.size();

    return 0;
}