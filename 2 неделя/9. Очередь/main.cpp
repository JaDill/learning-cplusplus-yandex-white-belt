#include <iostream>
#include <cstring>
#include <vector>
#include <algorithm>


int main()
{
    int n;
    std::cin >> n;
    std::vector<bool> a;
    int count;
    std::string command;
    for(int i=0;i<n;++i)
    {
        std::cin >> command;
        if (command == "WORRY")
        {
            std::cin >> count;
            a[count] = true;
        } else if (command == "QUIET")
        {
            std::cin >> count;
            a[count] = false;
        } else if (command == "COME")
        {
            std::cin >> count;
            a.resize(a.size() + count);
        } else
            std::cout << std::count(a.begin(), a.end(), true)<<"\n";
    }

    return 0;
}