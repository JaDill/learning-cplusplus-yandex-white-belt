#include <iostream>
#include <cstring>
#include <vector>

void MoveStrings(std::vector<std::string>& source, std::vector<std::string>& target)
{
    for (auto& str: source)
        target.push_back(str);
    source.clear();
}

int main()
{
    std::vector<std::string> a, b;
    MoveStrings(a, b);
    return 0;
}