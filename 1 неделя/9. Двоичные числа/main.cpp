#include <iostream>
#include <vector>
#include <algorithm>

int main()
{
    int a;
    std::vector<short> binary;
    std::cin >> a;
    while (a != 0)
    {
        binary.push_back(a % 2);
        a /= 2;
    }
    std::reverse(binary.begin(), binary.end());
    for (short b : binary)
        std::cout << b;
    return 0;
}