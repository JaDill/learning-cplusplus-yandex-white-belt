#include <iostream>
#include <cmath>

int main()
{
    double a, b, c;
    std::cin >> a >> b >> c;
    double d = b * b - 4 * a * c;
    if (a == 0)
    {
        if (b != 0)
            std::cout << -c / b;
    } else if (d == 0)
        std::cout << -b / (2 * a);
    else if (d > 0)
    {
        std::cout << (-b - sqrt(d)) / (2 * a) << " ";
        std::cout << (-b + sqrt(d)) / (2 * a);
    }
    return 0;
}