#include <iostream>

int nod(int a, int b)
{
    if(a==0)
        return b;

    b %= a;
    nod(b, a);
}

int main()
{
    int a, b;
    std::cin >> a >> b;
    std::cout << (a < b ? nod(a, b) : nod(b, a));
}