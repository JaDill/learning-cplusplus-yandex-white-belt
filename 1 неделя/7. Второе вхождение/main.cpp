#include <iostream>
#include <cstring>

int main()
{
    char ch = 'f';
    std::string str;
    std::cin >> str;
    int index = -2;
    for (int i = 0; i < str.size(); ++i)
        if (str[i] == ch && index == -2)
            index = -1;
        else if (str[i] == ch && index == -1)
            index = i;
    std::cout << index;
}